# Readme #

A generic library for using Smartech's Smart Response input devices.
Allows querying audiences from Python, C, C++, Matlab, and any other programming language on Windows by means of a DLL and wrapper script (for Python).

## Dependencies ##
- Install Microsoft Visual Studio 2010..2013 (higher versions weren't tested)
- Get and install `vcredist_x64.exe` or `vcredist_x86.exe` matching your system architecture and installed Visual Studio version
- Install `smartesi2013decusbv2.exe` (Full USB installer). Select Response+Drivers, deselect all galleries (try to avoid installing Ink/Notebook). `SmartResponseSDK11Win` is not necessary, because its `includes`, `.lib`, `.dll` and headers are already contained in this project repository
- Put `SMARTResponseSDK.dll` into `Debug/` and/or `Release/` folder(s)
- Put `SMARTResponseSDK.lib` into `srlib/lib`
- Put the following source files into `srlib/include`:
  - `baseapi.h`
  - `smartresponseapiversioning.h`
  - `smartresponsesdk.h`
  - `v1/class.h`
  - `v1/connection.h`
  - `v1/errorinfo.h`
  - `v1/features.h`
  - `v1/question.h`
  - `v1/questionset.h`
  - `v1/types.h`
- To continue development, read `Smart response SDK user guide.pdf`
- Add to system path `C:\Program Files (x86)\SMART Technologies\Education Software;C:\Program Files (x86)\SMART Technologies\SMART Notebook SE`
- Open the project file `srlib.sln` with Visual Studio and compile the binary

## Deployment ##
  * Install `MSVCR110.dll` or (`MSVCR120.dll`), depending on used Visual Studio version

## Ideas ##
- #define COUNT_OF(array) (sizeof(array) / sizof(array[0]))
- Compile the different library targets on [AppVeyor](https://ci.appveyor.com) or [CircleCI](https://circleci.com/)
