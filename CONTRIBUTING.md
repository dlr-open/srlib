# Contributing #

If you want to contribute to this project, please sign the [CLA form](DLR Individual Contributor License Agreement.docx) and send it to the project maintainer.
Once signed, you are welcome to suggest improvements via patches, issues, comments, pull requests/merge requests or other forms of contribution.
