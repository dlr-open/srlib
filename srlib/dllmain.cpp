// Copyright (c) 2018. Deutsches Zentrum fuer Luft- und Raumfahrt (DLR). All rights reserved.  http://www.dlr.de/ly
// This Source Code Form is subject to the terms of the Apache License, Version 2.0. If a copy of the license was not distributed with this file, You can obtain one at http://www.apache.org/licenses/LICENSE-2.0

#include "stdafx.h"

// DLL main entry point
BOOL APIENTRY DllMain(HMODULE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved) {
	switch (ul_reason_for_call) {
	case DLL_PROCESS_ATTACH:
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
	break;
	}
	return TRUE;
}
