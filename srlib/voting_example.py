# Copyright (c) 2018. Deutsches Zentrum fuer Luft- und Raumfahrt (DLR). All rights reserved.  http://www.dlr.de/ly
# This Source Code Form is subject to the terms of the Apache License, Version 2.0. If a copy of the license was not distributed with this file, You can obtain one at http://www.apache.org/licenses/LICENSE-2.0

import srlib

if __name__ == '__main__':
  ''' Simple voting example. '''
  print("\nRead out loud: Please decide if the mass assessment is either  *too low* (A),  *just right* (B),  or *too high* (C)!\n")

  ans = srlib.multichoice("Plz_Vote", "Your estimate A=2Low, B=OK, C=2High", ["A", "B", "C"], [], timeout = 300)

  with open(r"D:\voting.csv", "w") as fd:
    fd.write("Too low;Just right;Too high\n%d;%d;%d\n" % tuple([len([_ for _ in ans.values() if _ == option]) for option in "ABC"]))
