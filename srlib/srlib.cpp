// Copyright (c) 2018. Deutsches Zentrum fuer Luft- und Raumfahrt (DLR). All rights reserved.  http://www.dlr.de/ly
// This Source Code Form is subject to the terms of the Apache License, Version 2.0. If a copy of the license was not distributed with this file, You can obtain one at http://www.apache.org/licenses/LICENSE-2.0

#include "stdafx.h"  // DLL Windows-API
#include "srlib.h"
#include <sys/stat.h>
#include <iostream>
#include <fstream>
#include <string>
#include <map>

// TODO fix handling of unicode strings (using size + 1 instead of real binary length (since UTF-8 length may differ))

// Style guide:
//   C++-style functions allow for default arguments, C-style not, but only in the implementation, not in the header
//   C-style strings are given with char* and length, arrays with char**, int* of lengths, and length


// global constants
const int DEFAULT_TIMEOUT = 30; // seconds for common operations (excluding actual answering waiting time)

// global variables TODO move into a Connection class
smartresponse_connectionV1_t *connection;
bool connected;
bool pluggedIn;
bool ready;
bool classStarted;
bool inQuestionSet;
bool questionStarted;
bool allAnswered;
int numberOfStudents;
string interruptMarkerFile;
map<string, string> answers; // contains all users' choices user ID -> string of answer  TODO use template for return type (e.g. bool, string, double)

// global listener
smartresponse_listener_t *cb_con;
smartresponse_listener_t *cb_dis;
smartresponse_listener_t *cb_cfl;
smartresponse_listener_t *rpi;
smartresponse_listener_t *rpo;
smartresponse_listener_t *rrd;
smartresponse_listener_t *csa;
smartresponse_listener_t *cso;
smartresponse_listener_t *cfs;
smartresponse_listener_t *cft;
smartresponse_listener_t *qs;
smartresponse_listener_t *qt;
smartresponse_listener_t *qsf;
smartresponse_listener_t *qtf;
smartresponse_listener_t *qss;
smartresponse_listener_t *qst;
smartresponse_listener_t *qssf;
smartresponse_listener_t *qstf;
smartresponse_listener_t *clc;
smartresponse_listener_t *cld;
smartresponse_listener_t *cqd;
smartresponse_listener_t *cqc;
smartresponse_listener_t *clr;


// Helper functions
const bool addStudent(smartresponse_classV1_t* signInClass, const string lastName, const string firstName, const string studentId) {
	const sr_student_t *student = sr_student_create(lastName.c_str(), lastName.size() + 1, firstName.c_str(), firstName.size() + 1, studentId.c_str(), studentId.size() + 1);
	const SR::ADD_STUDENT_STATUS status = sr_class_addstudent(signInClass, const_cast<sr_student_t*>(student));
	// alternatively: sr_add_student_to_class(signInClass, const_cast<sr_student_t*>(student);
	sr_student_release(const_cast<sr_student_t*>(student));
	return status != SR::ADD_STUDENT_STATUS::OK; // or SR:OK, ID_IN_USE, CLASS_OR_STUDENT_IS_NULL
}

void removeStudent(smartresponse_classV1_t* signInClass, const string studentId) {
	sr_class_removestudentwithid(signInClass, const_cast<char*>(studentId.c_str()));
}

/* Helper to check for file existence of the interrupt marker. */
const bool markerFileExists(void) {
	struct stat _stat;
	return 0 == stat(interruptMarkerFile.c_str(), &_stat); // no error means file exists
}

/* Helper that waits either a maximum set amount of time, a keyboard input (e.g. from the calling code), or for a certain flag to be set (hopefully atomic access). */
void waitFor(const bool *conditionFlag, const int timeoutInS = DEFAULT_TIMEOUT, const bool desiredState = true) {
	int count = 10 * timeoutInS; // enable more granular interruption
	while ((count > 0) && (*conditionFlag != desiredState) && (count % 10 != 9 || !markerFileExists())) { // exits when countdown zero, or flag in desired state
		Sleep(100); // give time
		count--;
	}
}


/* Helper to retrieve the connected device ID (e.g. PE, LE, XE). */
const int getHardwareMode(void) {
	return sr_connection_get_current_mode(connection);
}

/* Helper to gather driver details and limits. */
void printHardwareProperties(void) {
	const int retval = smartresponse_sdk_initialize(0x200); // returns 0..512 TODO move outside this function?
	// Get and display driver features
	const int numericLen = sr_features_numericanswerlength();
	const int textLen = sr_features_textanswerlength();
	const int number = sr_features_maxquestionsperquestionset(); // no. of questions (a question set can contain a maximum of 40 questions)
	int maxChoiceNumber, minChoiceNumber, maxSelectionNumber, minSelectionNumber;
	sr_features_questionchoicenumber(&maxChoiceNumber, &minChoiceNumber); // questions choices
	sr_features_questionselectionnumber(&maxSelectionNumber, &minSelectionNumber);
	printf("  * Min/Max. no. choices for multiple choice: %d/%d\n", minChoiceNumber, maxChoiceNumber);
	printf("  * Min/Max. no. choices for selections: %d/%d\n", minSelectionNumber, maxSelectionNumber);
	printf("  *     Max. no. questions: %d\n", number);
	printf("  *     Max. no. characters for numerics/texts: %d/%d\n", numericLen, textLen);
	printf("  *     Max. no. characters for classroom name: 8\n");
	sr_sdk_terminate();
}

/* Helper to open the classroom before voting. */
const smartresponse_classV1_t* setUpClassroom(const string classroomName, const vector<string> studentIds, const bool anonymous = false, const int timeout = DEFAULT_TIMEOUT) {
	printf("Initializing...\n");
	connected = false; // TODO move to Connection constructor
	pluggedIn = false;
	ready = false;
	classStarted = false;
	inQuestionSet = false;
	questionStarted = false;
	allAnswered = false;
	numberOfStudents = studentIds.size(); // remember participating students to know when all have answered TODO move to Connection class
	answers.clear();

	const int retval = smartresponse_sdk_initialize(0x200); // returns 0..512 TODO move outside this function?

	// Install listeners for error and event handling
	printf("Installing listeners...\n"); // manual says to connect through sr_connection_construct(); but we got error code -3/-4 from that.
	connection = sr_connection_create(SMARTRESPONSE_INVOKE_CALLBACKS_ON_BACKGROUND_THREADS); // alternative: SMARTRESPONSE_INVOKE_CALLBACKS_ON_MAIN_THREAD_ONLY);

	// ... teacher-side callbacks (0: a context, could also be "this")
	smartresponse_listener_t *cb_con = sr_connection_listenonconnected(connection, onConnect, 0);
	smartresponse_listener_t *cb_dis = sr_connection_listenondisconnected(connection, onDisconnect, 0);
	smartresponse_listener_t *cb_cfl = sr_connection_listenonconnectiondidfail(connection, onConnectionFail, 0);
	smartresponse_listener_t *rpi = sr_connection_listenonreceiverpluggedin(connection, onReceiverPluggedIn, 0);
	smartresponse_listener_t *rpo = sr_connection_listenonreceiverunplugged(connection, onReceiverPluggedOut, 0);
	smartresponse_listener_t *rrd = sr_connection_listenonreceiverready(connection, onReceiverReady, 0);
	smartresponse_listener_t *csa = sr_connection_listenonclassstarted(connection, onClassStart, 0);
	smartresponse_listener_t *cso = sr_connection_listenonclassstopped(connection, onClassStop, 0);
	smartresponse_listener_t *cfs = sr_connection_listenonclassfailtostart(connection, onClassStartFail, 0);
	smartresponse_listener_t *cft = sr_connection_listenonclassfailtostop(connection, onClassStopFail, 0);
	smartresponse_listener_t *qs = sr_connection_listenonquestionstarted(connection, onQuestionStart, 0);
	smartresponse_listener_t *qt = sr_connection_listenonquestionstopped(connection, onQuestionStop, 0);
	smartresponse_listener_t *qsf = sr_connection_listenonquestionfailtostart(connection, onQuestionStartFail, 0);
	smartresponse_listener_t *qtf = sr_connection_listenonquestionfailtostop(connection, onQuestionStopFail, 0);
	smartresponse_listener_t *qss = sr_connection_listenonquestionsetstarted(connection, onQuestionsetStart, 0);
	smartresponse_listener_t *qst = sr_connection_listenonquestionsetstopped(connection, onQuestionsetStop, 0);
	smartresponse_listener_t *qssf = sr_connection_listenonquestionsetfailtostart(connection, onQuestionsetStartFail, 0);
	smartresponse_listener_t *qstf = sr_connection_listenonquestionsetfailtostop(connection, onQuestionsetStopFail, 0);

	// ... remote state change callback
	smartresponse_listener_t  *clc = sr_connection_listenonclickerconnected(connection, onClickerConnect, 0); // remote signed in
	smartresponse_listener_t  *cld = sr_connection_listenonclickerdisconnected(connection, onClickerDisconnect, 0); // remote signed out
	smartresponse_listener_t  *cqd = sr_connection_listenonclickerquestioned(connection, onQuestion, 0); // question raised
	smartresponse_listener_t  *cqc = sr_connection_listenonclickerquestioncanceled(connection, onQuestionCancel, 0); // question down

	// ... remote response callback
	smartresponse_listener_t  *clr = sr_connection_listenonclickerresponded(connection, onClickerResponse, 0); // assessment submitted

	// Start actual classroom setup
	printf("Connecting to device...\n");
	sr_connection_connect(connection);
	waitFor(&connected, timeout);
	waitFor(&ready, timeout);

	printf("Setting up classroom...\n");
	sr_set_classroom_name(connection, const_cast<char*>(classroomName.c_str()), classroomName.size() + 1);
	const smartresponse_classV1_t* currentClass = sr_connection_currentclass(connection);
	if (currentClass) {
		printf("There was already a class started. Stopping it now. This may take a while.\n");
		sr_connection_stopclass(connection);
		waitFor(&classStarted, timeout, false);
	}

	const smartresponse_classV1_t* classroom = sr_class_create(const_cast<char*>(classroomName.c_str()), classroomName.size() + 1, /* anonymously: MACs as IDs */ anonymous);
	if (!anonymous) {
		printf("Adding students...\n");
		for (auto id = studentIds.begin(); id != studentIds.end(); ++id) {
			printf("  * %s\n", id->c_str());
			if (addStudent(const_cast<smartresponse_classV1_t*>(classroom), /* last name */ "User", /* first name (shown) */ const_cast<char *>(id->c_str()), /* id */ const_cast<char*>(id->c_str()))) {
				printf("Error adding student. ID in use or connection corrupt.");
			}
		}
	}

	printf("Starting class...\n");
	sr_connection_startclass(connection, const_cast<smartresponse_classV1_t*>(classroom));
	waitFor(&classStarted, timeout);
	printf("Finished setup.\n");
	return classroom;
}

/* Helper to close the classroom after voting. */
void tearDownClassroom(const smartresponse_classV1_t* classroom, const vector<string> studentIds, const int timeout = DEFAULT_TIMEOUT) {
	if (classroom) {
		if (studentIds.size() > 0) printf("Removing students...\n");
		for (auto id = studentIds.begin(); id != studentIds.end(); ++id) {
			printf("  * %s\n", id->c_str());
			removeStudent(const_cast<smartresponse_classV1_t*>(classroom), id->c_str());
		}
	}

	printf("Stopping class...\n");
	sr_connection_stopclass(connection);
	waitFor(&classStarted, timeout, false);
	printf("Releasing class...\n");
	sr_class_release(const_cast<smartresponse_classV1_t*>(classroom));
	printf("Disconnecting...\n");
	sr_connection_disconnect(connection);
	waitFor(&connected, timeout, false);

	printf("Releasing listeners...\n");
	sr_listener_release(rrd);
	sr_listener_release(rpo);
	sr_listener_release(rpi);
	sr_listener_release(csa);
	sr_listener_release(cso);
	sr_listener_release(qstf);
	sr_listener_release(qssf);
	sr_listener_release(qst);
	sr_listener_release(qss);
	sr_listener_release(qtf);
	sr_listener_release(qsf);
	sr_listener_release(qt);
	sr_listener_release(qs);
	sr_listener_release(cft);
	sr_listener_release(cfs);
	sr_listener_release(clc);
	sr_listener_release(cld);
	sr_listener_release(cqd);
	sr_listener_release(cqc);
	sr_listener_release(clr);
	sr_listener_release(cb_cfl);
	sr_listener_release(cb_dis);
	sr_listener_release(cb_con);

	printf("Releasing connection...\n");
	sr_connection_release(connection);
	printf("Terminating...\n");
	sr_sdk_terminate();
	printf("Finished.\n");
}


/* Helper to list all gathered answers. */
void showAnswers(void) {
	ofstream csv; // for file output (reusing the marker file)
	csv.open(interruptMarkerFile); // open for writing
	printf("Answers summary:\n");
	printf("User;Answer\n");
	csv << "User;Answer\n";
	for (auto p = answers.begin(); p != answers.end(); ++p) {
		printf("%s;%s\n", p->first.c_str(), p->second.c_str());
		csv << p->first << ";" << p->second << "\n";
	}
	csv.close();
}

/* Helper to add choices to the question. */
void addChoices(const smartresponse_questionV1_t* question, const vector<string> choices) {
	printf("Adding choices...\n");
	for (auto choice = choices.begin(); choice != choices.end(); ++choice) {
		sr_question_setchoicetext(const_cast<smartresponse_questionV1_t*>(question), INDEX(choices, choice), const_cast<char*>(choice->c_str()), choice->size() + 1);
		printf("  * %s\n", choice->c_str());
		//sr_question_setchoicelabel(const_cast<smartresponse_questionV1_t*>(question), INDEX(choices, choice), &charlabel);
	}
}

/* C interface for choice voting. */
const int multichoice1(const char* markerFile, const char* classroomName, const int clen, const char* questionText, const int qlen, const char** choices, const int* clens, const int clennum, const char** studentIds, const int* slens, const int slennum, const int timeout, const bool allowShortcut) {
	printf("Using interrupt marker file '%s'\n", markerFile);
	vector<string> _choices;
	for (int i = 0; i < clennum; i++) _choices.push_back(string(choices[i], clens[i]));
	vector<string> _studentIds;
	for (int i = 0; i < slennum; i++) _studentIds.push_back(string(studentIds[i], slens[i]));
	return multichoice(string(markerFile), string(classroomName, clen), string(questionText, qlen), _choices, _studentIds, timeout, allowShortcut);
}

/* C++ interface for choice voting. */
const int multichoice(const string markerFile, const string classroom, const string questionText, const vector<string> choices, const vector<string> studentIds, const int timeout = DEFAULT_TIMEOUT, const bool allowShortcut = true) {
	interruptMarkerFile = markerFile;
	const smartresponse_classV1_t* _classroom = setUpClassroom(classroom, studentIds, studentIds.size() == 0, DEFAULT_TIMEOUT);

	const smartresponse_questionV1_t *question = sr_question_create(SMARTRESPONSE_QUESTIONTYPE_MULTIPLECHOICE, choices.size());
	printf("Starting question...\n");
	sr_connection_startquestion(connection, const_cast<smartresponse_questionV1_t*>(question));
	waitFor(&questionStarted, DEFAULT_TIMEOUT);

	sr_question_setquestiontext(const_cast<smartresponse_questionV1_t*>(question), const_cast<char*>(questionText.c_str()), questionText.size() + 1);
	addChoices(question, choices);

	printf("Waiting for answers...\n");
	waitFor(allowShortcut ? &allAnswered : &allowShortcut, timeout); // wait for all answered on shortcut, otherwise wait until timeout (will never be true)

	printf("Stopping question...\n");
	sr_connection_stopquestion(connection);
	waitFor(&questionStarted, DEFAULT_TIMEOUT, false);

	printf("Releasing question...\n");
	sr_question_release(const_cast<smartresponse_questionV1_t*>(question));

	tearDownClassroom(const_cast<smartresponse_classV1_t*>(_classroom), studentIds, DEFAULT_TIMEOUT);
	showAnswers();
	printf("Returning to caller\n");
	return answers.size(); // so clients can check if all participated
}

/* C interface for multi answer voting. */
const int multiselect1(const char* markerFile, const char* classroomName, const int clen, const char* questionText, const int qlen, const char** choices, const int* clens, const int clennum, const char** studentIds, const int* slens, const int slennum, const int timeout, const unsigned int maxAnswersNum, const bool allowShortcut) {
	printf("Using interrupt marker file '%s'\n", markerFile);
	vector<string> _choices;
	for (int i = 0; i < clennum; i++) _choices.push_back(string(choices[i], clens[i]));
	vector<string> _studentIds;
	for (int i = 0; i < slennum; i++) _studentIds.push_back(string(studentIds[i], slens[i]));
	return multiselect(string(markerFile), string(classroomName, clen), string(questionText, qlen), _choices, _studentIds, timeout, maxAnswersNum, allowShortcut);
}

/* C++ interface for multi answer voting. */
const int multiselect(const string markerFile, const string classroomName, const string questionText, const vector<string> choices, vector<string> studentIds, const int timeout, const unsigned int maxAnswersNum = 0, const bool allowShortcut = true) {
	interruptMarkerFile = markerFile;
	const smartresponse_classV1_t* _classroom = setUpClassroom(classroomName, studentIds, studentIds.size() == 0, DEFAULT_TIMEOUT);
	const smartresponse_questionV1_t *question = sr_question_create(SMARTRESPONSE_QUESTIONTYPE_MULTIPLEANSWER, maxAnswersNum == 0 ? choices.size() : min(choices.size(), maxAnswersNum));

	printf("Starting question...\n");
	sr_connection_startquestion(connection, const_cast<smartresponse_questionV1_t*>(question));
	waitFor(&questionStarted, DEFAULT_TIMEOUT);

	sr_question_setquestiontext(const_cast<smartresponse_questionV1_t*>(question), const_cast<char*>(questionText.c_str()), questionText.size() + 1);
	//sr_question_setanswer(const_cast<smartresponse_questionV1_t*>(question), const_cast<char*>(answerText.c_str()), answerText.size() + 1);
	//sr_question_setquestionpoints(const_cast<smartresponse_questionV1_t*>(question), 1);
	addChoices(question, choices);

	printf("Waiting for answers...\n");
  waitFor(allowShortcut ? &allAnswered : &allowShortcut, timeout); // wait for all answered on shortcut, otherwise wait until timeout (will never be true)

	printf("Stopping question...\n");
	sr_connection_stopquestion(connection);
	waitFor(&questionStarted, DEFAULT_TIMEOUT, false);

	printf("Releasing question...\n");
	sr_question_release(const_cast<smartresponse_questionV1_t*>(question));

	tearDownClassroom(const_cast<smartresponse_classV1_t*>(_classroom), studentIds, DEFAULT_TIMEOUT);
	showAnswers();
	printf("Returning to caller\n");
	return (const int)answers.size();
}

/* C interface for yes/no voting. */
const int yesno1(const char* markerFile, const char* classroom, const int clen, const char* questionText, const int qlen, const char** studentIds, const int* slens, const int slennum, const int timeout, const bool allowShortcut) {
	printf("Using interrupt marker file '%s'\n", markerFile);
	vector<string> _studentIds;
	for (int i = 0; i < slennum; i++) _studentIds.push_back(string(studentIds[i], slens[i]));
	return yesno(string(markerFile), string(classroom, clen), string(questionText, qlen), _studentIds, timeout, allowShortcut);
}

/* C++ interface for yes/no voting. */
const int yesno(const string markerFile, const string classroom, const string questionText, const vector<string> studentIds, const int timeout, const bool allowShortcut = true) {
	interruptMarkerFile = markerFile;
	const smartresponse_classV1_t* _classroom = setUpClassroom(classroom, studentIds, studentIds.size() == 0, DEFAULT_TIMEOUT);
	const smartresponse_questionV1_t *question = sr_question_create(SMARTRESPONSE_QUESTIONTYPE_YESNO, /* if 0 not working despite doc saying otherwise */ 2); // alternatively use TRUEFALSE

	printf("Starting question...\n");
	sr_connection_startquestion(connection, const_cast<smartresponse_questionV1_t*>(question));
	waitFor(&questionStarted, DEFAULT_TIMEOUT);

	sr_question_setquestiontext(const_cast<smartresponse_questionV1_t*>(question), const_cast<char*>(questionText.c_str()), questionText.size() + 1);
	waitFor(allowShortcut ? &allAnswered : &allowShortcut, timeout); // wait for all answered on shortcut, otherwise wait until timeout (will never be true)

	printf("Stopping question...\n");
	sr_connection_stopquestion(connection);
	waitFor(&questionStarted, DEFAULT_TIMEOUT, false);

	printf("Releasing question...\n");
	sr_question_release(const_cast<smartresponse_questionV1_t*>(question));

	tearDownClassroom(const_cast<smartresponse_classV1_t*>(_classroom), studentIds, DEFAULT_TIMEOUT);
	showAnswers();
	printf("Returning to caller\n");
	return (const int)answers.size();
}

/* C interface for decimal voting. */
const int decimal1(const char* markerFile, const char* classroomName, const int clen, const char* questionText, const int qlen, const char** studentIds, const int * slens, const int slennum, const int timeout, const bool allowShortcut) {
	printf("Using interrupt marker file '%s'\n", markerFile);
	vector<string> _studentIds;
	for (int i = 0; i < slennum; i++) _studentIds.push_back(string(studentIds[i], slens[i]));
	return decimal(string(markerFile), string(classroomName, clen), string(questionText, qlen), _studentIds, timeout, allowShortcut);
}

/* C++ interface for decimal voting. */
const int decimal(const string markerFile, const string classroom, const string questionText, const vector<string> studentIds, const int timeout, const bool allowShortcut = true) {
	interruptMarkerFile = markerFile;
	const smartresponse_classV1_t* _classroom = setUpClassroom(classroom, studentIds, studentIds.size() == 0, DEFAULT_TIMEOUT);
	const smartresponse_questionV1_t *question = sr_question_create(SMARTRESPONSE_QUESTIONTYPE_DECIMAL, 0); // to create a single question

	printf("Starting question...\n");
	sr_connection_startquestion(connection, const_cast<smartresponse_questionV1_t*>(question));
	waitFor(&questionStarted, DEFAULT_TIMEOUT);

	sr_question_setquestiontext(const_cast<smartresponse_questionV1_t*>(question), const_cast<char*>(questionText.c_str()), questionText.size() + 1);
  waitFor(allowShortcut ? &allAnswered : &allowShortcut, timeout); // wait for all answered on shortcut, otherwise wait until timeout (will never be true)

	printf("Stopping question...\n");
	sr_connection_stopquestion(connection);
	waitFor(&questionStarted, DEFAULT_TIMEOUT, false);

	printf("Releasing question...\n");
	sr_question_release(const_cast<smartresponse_questionV1_t*>(question));

	tearDownClassroom(const_cast<smartresponse_classV1_t*>(_classroom), studentIds, DEFAULT_TIMEOUT);
	showAnswers();
	printf("Returning to caller\n");
	return answers.size();
}

// teacher-side callbacks
static void __stdcall onConnect(void *arg) { printf("<Connected>\n"); connected = true; }
static void __stdcall onDisconnect(void *arg) { printf("<Disconnected>\n"); connected = false;  }
static void __stdcall onConnectionFail(void *arg) { printf("<Connection failed>\n"); printError(); }
static void __stdcall onReceiverPluggedIn(void *arg) { printf("<Receiver plugged in>\n"); pluggedIn = true; }
static void __stdcall onReceiverPluggedOut(void *arg) { printf("<Receiver plugged out>\n"); pluggedIn = false; ready = false; }
static void __stdcall onReceiverReady(void *arg) { printf("<Receiver ready>\n"); ready = true; }
static void __stdcall onClassStart(void *arg) { printf("<Class started>\n"); classStarted = true; }
static void __stdcall onClassStop(void *arg) { printf("<Class stopped>\n"); classStarted = false; }
static void __stdcall onClassStartFail(void *arg) { printf("<Class start failed>\n"); printError(); }
static void __stdcall onClassStopFail(void *arg) { printf("<Class stop failed>\n"); printError(); }
static void __stdcall onQuestionStart(void *arg) {printf("<Question started>\n"); questionStarted = true; }
static void __stdcall onQuestionStop(void *arg) { printf("<Question stopped>\n"); questionStarted = false; }
static void __stdcall onQuestionStartFail(void *arg) { printf("<Question start failed>\n"); printError(); }
static void __stdcall onQuestionStopFail(void *arg) { printf("<Question stop failed>\n"); printError(); }
static void __stdcall onQuestionsetStart(void *arg) { printf("<Questionset started>\n"); inQuestionSet = true; }
static void __stdcall onQuestionsetStop(void *arg) { printf("<Questionset stopped>\n"); inQuestionSet = true; }
static void __stdcall onQuestionsetStartFail(void *arg) { printf("<Questionset start failed>\n"); printError(); }
static void __stdcall onQuestionsetStopFail(void *arg) { printf("<Questionset stop failed>\n"); printError(); }
static void __stdcall onModeSwitch(void *arg) { printf("<Mode switched>\n"); }
static void __stdcall onModeSwitchFail(void *arg) { printf("<Mode switch failed>\n"); printError(); }

// Remote state change callback
static void __stdcall onClickerConnect(char* studentId, void *arg) { printf("<Clicker connected> %s\n", studentId); connected = true; }
static void __stdcall onClickerDisconnect(char *studentId, void *arg) { printf("<Clicker disconnected> %s\n", studentId); connected = false; }
static void __stdcall onQuestion(char *studentId, void *arg) { printf("<Clicker questioned> %s\n", studentId); questionStarted = true; }
static void __stdcall onQuestionCancel(char *studentId, void *arg) { printf("<Clicker question cancelled> %s\n", studentId); questionStarted = false; }
static void __stdcall onSubmit(char *studentId, void *arg) { printf("<Clicker submitted> %s\n", studentId); }

// Remote response callback
static void __stdcall onClickerResponse(char* studentId, char *questionId, char* answer, void *args) {
	printf("  Q: %s  S: %s  A: %s\n", questionId, studentId, answer[0] == '\0' ? "<none>" : answer);
	answers[string(studentId)] = string(answer[0] == '\0' ? "<none>" : answer); // store answer
	allAnswered = answers.size() == numberOfStudents;
	if (allAnswered) printf("All answers received\n");
}

/* Get error object. */
const smartresponse_errorinfoV1_t* getError(void) {
	return sr_connection_copyerrorinfo(connection); // to query an error
}

/* Release error resource. */
void releaseError(const smartresponse_errorinfoV1_t* error) {
	smartresponse_errorinfoV1_release(const_cast<smartresponse_errorinfoV1_t*>(error)); // clean error status
}

/* Retrieve the status code */
const int getErrorCode(const smartresponse_errorinfoV1_t *error) {
	return error ? sr_errorinfo_statuscode(const_cast<smartresponse_errorinfoV1_t*>(error)) : 0;
}

/* Get lengths of required string buffers for error messages. */
void getErrorLengthes(const smartresponse_errorinfoV1_t *error, unsigned int *statusSize, unsigned int *messageSize) {
	*statusSize = error ? sr_errorinfo_statustext(const_cast<smartresponse_errorinfoV1_t*>(error), NULL, 0) : 0; // to find the required char* size of the status text
	*messageSize = error ? sr_errorinfo_fulldetails(const_cast<smartresponse_errorinfoV1_t*>(error), NULL, 0) : 0; // NULL means "query size"
}

/* Return encoded strings describing the error */
void getErrorStatus(const smartresponse_errorinfoV1_t *error, char *statusText, char *detailText) {
	if (error) {
		unsigned int statusSize, detailSize;
		getErrorLengthes(error, &statusSize, &detailSize);
		sr_errorinfo_statustext(const_cast<smartresponse_errorinfoV1_t*>(error), statusText, statusSize);
		sr_errorinfo_fulldetails(const_cast<smartresponse_errorinfoV1_t*>(error), detailText, detailSize);
	}
}

/* Print error details (no returning to calling application). */
void printError(void) {
	const smartresponse_errorinfoV1_t *error = getError();
	if (error) {
		const int statusCode = getErrorCode(error);
		unsigned int statusSize, detailSize;
		getErrorLengthes(error, &statusSize, &detailSize);
		char* statusText = new char[statusSize];
		char* detailText = new char[detailSize];
		getErrorStatus(error, statusText, detailText);
		printf("Connection to software service failed. The status code is %i, the status text is \"%s\" and the full details is \"%s\"\n", statusCode, statusText, detailText);
		delete statusText;
		delete detailText;
	}
	releaseError(error);
}
