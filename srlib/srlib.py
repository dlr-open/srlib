# Copyright (c) 2018. Deutsches Zentrum fuer Luft- und Raumfahrt (DLR). All rights reserved.  http://www.dlr.de/ly
# This Source Code Form is subject to the terms of the Apache License, Version 2.0. If a copy of the license was not distributed with this file, You can obtain one at http://www.apache.org/licenses/LICENSE-2.0

from ctypes import byref, CDLL, c_bool, c_char_p, c_int, c_void_p
import os
import sys
import threading

# Determine user home directory
try:
  import appdirs  # optional dependency: https://github.com/ActiveState/appdirs
  home = appdirs.user_data_dir("srlib", "DLR")  # app/author
except:  # noqa: E722
  try:  # get user home regardless of currently set environment variables
    from win32com.shell import shell, shellcon
    home = shell.SHGetFolderPath(0, shellcon.CSIDL_PROFILE, None, 0)
  except:  # noqa: E722
    try:  # unix-like native solution ignoring environment variables
      from pwd import getpwuid
      home = getpwuid(os.getuid()).pw_dir
    except:  # now try standard approaches  # noqa: E722
      home = os.getenv("USERPROFILE")  # for windows only
      if home is None: home = os.expanduser("~")  # recommended cross-platform solution, but could refer to a mapped network drive on Windows, which may not what you want
try: assert home  # if assertion fails, we cannot determine user's home directory in this environment
except: home = r"C:" if sys.platform == 'win32' else '/tmp'  # noqa: E722
interruptMarkerFile = os.path.join(home, '.srlib_interrupt_marker')


dll = CDLL(os.path.join(os.path.dirname(__file__), '..', 'Release', 'srlib'))  # access library

DEFAULT_TIMEOUT = 45  # seconds
ALLOWED_CHARS = (''.join([''.join([chr(l) for l in lists]) for lists in [list(range(ord('A'), ord('Z')+1)) + list(range(ord('a'), ord('z')+1)) + list(range(ord('0'), ord('9')+1))]])) + "`-=!@#$%&*()_+[];':\\\",./<? "  # according to manual
DIGITS = '0123456789'
CHOICE_LABELS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"[:10]  # free selection only available in VE mode
MAXDECIMAL = 10
MAXTEXT = 20  # TODO use clicker information instead of hardcoding here


def _onlyDigits(string):
  return all([d in DIGITS for d in string])


def _encode_for_c(thestring):
  return str.encode(thestring) if type(thestring) is str else thestring


def _decode_from_c(thestring):
  return thestring.decode("utf-8") if sys.version_info[0] >= 3 else thestring


def _sanitizeClassroomName(name):
  ''' Remove all disallowed characters for classroom names.

  >>> print _sanitizeClassroomName("abcABC098")
  abcABC09
  >>> print _sanitizeClassroomName(" *!" + r'"$\\3' + "'")
   *!"$\\3'
  '''
  return ''.join([c for c in name if c in ALLOWED_CHARS][:8])


def _sanitizeQuestionText(name):
  ''' Remove all disallowed characters for classroom names.

  >>> print _sanitizeQuestionText("abcA BC098*!\\"$\\'|3")
  abcA BC098*!"$'3
  '''
  return ''.join([c for c in name if c in ALLOWED_CHARS][:MAXTEXT])


def _sanitizeChoice(choice):
  ''' Remove all disallowed characters for classroom names.

  >>> print _sanitizeChoice("abcde12$23fgh ijk")
  ABCDEFGHIJ
  '''
  return ''.join([c for c in choice.upper() if c in CHOICE_LABELS])


def getHardwareMode():
  ''' Basic callback to detect connected hardware receiver(s).

      returns: 0..512
  >>> print getHardwareMode()
  8
  '''
  return dll.getHardwareMode()


def getHardwareModeString():
  ''' Human-readable form of the hard ware detection.

  >>> print getHardwareModeString()
  (8, 'SMART Response PE')
  '''
  mode = getHardwareMode()
  try:
    return (mode, {
            0: 'No SMART Response installed',
            1: 'SMART Response / Senteo software',
            2: 'SMART Response CE',
            4: 'SMART Response LE',
            8: 'SMART Response PE',
           16: 'SMART Response XE',
           32: 'SMART Response VE',
           64: 'Multiple devices without VE',
          128: 'Multiple devices with VE',
          256: 'Multiple devices without VE but with text',
          512: 'Multiple devices with VE and text'}[mode])
  except:  # noqa: E722
    return (-1, 'unexpected return code')


def printHardwareProperties():
  ''' List input limitations. '''
  dll.printHardwareProperties()


def _parseAnswers():
  ''' Read the intermediate results file. '''
  with open(interruptMarkerFile, "r") as csv:
    return {k: v for k, v in [row.split(";") for row in csv.read().split("\n")[:-1]] if k != 'User'}


def _interruptableDllCall(func, *args):
  ''' Interrupt current voting. Experiments with sending (or recognizing) data on STDIN failed, working with threads and queues seem to complex; therefore a simple file-based solution. '''
  if os.path.exists(interruptMarkerFile): os.unlink(interruptMarkerFile)  # ensure marker file is removed
  process = threading.Thread(target = func, args = [c_char_p(interruptMarkerFile)] + list(args))  # func only returns answers dict size, can be ignored
  process.start()  # start DLL call
  try:
    while True:
      process.join(.1)
      if not process.isAlive(): break
      sys.stdout.write("."); sys.stdout.flush()  # try to join (if already finished), otherwise repeat waiting
  except KeyboardInterrupt:  # external interruption of voting
    print("Creating marker file to interrupt DLL process")
    with open(interruptMarkerFile, "w"): pass  # create empty marker file
    print("Waiting to join DLL thread...")
    process.join()
  print("DLL process exited. Now parsing answers")
  return _parseAnswers()


def _makeCharP(string):
  ''' Provide a ctypes helper. '''
  c = c_char_p()
  c.value = _encode_for_c(string)
  return c


def _makeCArray(array):
  ''' Provide a ctypes helper. '''
  Strings = c_char_p * len(array)
  StringLengths = c_int * len(array)
  strings = Strings(*[_makeCharP(s) for s in array])
  lengths = StringLengths(*[c_int(len(s) + 1) for s in array])
  return (strings, lengths)


def multichoice(classroomName, questionText, choices, userNames = [], timeout = 30, shortcut = False):
  ''' Allow the user to select one out of multiple choices.

      classroomName: string (not shown on PE)
      questionText: string (not shown on PE)
      choices: [string] texts to provide for each option A..J (not shown on PE) minimum 2 (A/B), maximum 10 (A..J) TODO must be A..J (strange)
      userNames: [string] user IDs (should only containing characters 0..9 for PE to allow entering it). Empty list can be used for anonymous voting
      timeout: maximum waiting time in seconds until the question is closed
      shortcut: if true, always wait until timeout, otherwise return as soon as all participants have entered an answer (correction only possible until last has voted)
      returns: answersDict
  '''
  assert 1 <= len(_sanitizeClassroomName(classroomName)) <= 8
  assert 2 <= len(_sanitizeQuestionText(questionText)) <= MAXTEXT
  assert 2 <= len(choices) <= 10
  assert all([len(choice) == 1 and choice in CHOICE_LABELS for choice in choices])
  assert all([1 <= len(user) <= 15 and _onlyDigits(user) for user in userNames])
  _classroom = c_char_p(_sanitizeClassroomName(classroomName))
  _question = c_char_p(_sanitizeQuestionText(questionText))
  _choices, _ichoices = _makeCArray([_sanitizeChoice(choice) for choice in choices])
  _users, _iusers = _makeCArray(userNames)
  return _interruptableDllCall(dll.multichoice1, _classroom, c_int(len(classroomName) + 1), _question, c_int(len(questionText) + 1), _choices, _ichoices, c_int(len(choices)), _users, _iusers, c_int(len(userNames)), c_int(timeout), c_bool(shortcut))


def multiselect(classroomName, questionText, choices, userNames = [], concurrentAnswers = 0, timeout = DEFAULT_TIMEOUT, shortcut = False):
  ''' Allows the user to select several options out of multiple choices.
      Parameters are same as for multichoice, plus:
      concurrentAnswers: define a maximum number of distinct answers that each users can select. 0 means all, 1 reduces functionality to multichoice, maximum 6.
  '''
  assert 1 <= len(_sanitizeClassroomName(classroomName)) <= 8  # from manual
  assert 2 <= len(_sanitizeQuestionText(questionText)) <= MAXTEXT
  assert 2 <= len(choices) <= 10
  assert 0 <= concurrentAnswers <= 6
  assert all([len(choice) == 1 and choice in CHOICE_LABELS for choice in choices])
  assert all([1 <= len(user) <= 15 and _onlyDigits(user) for user in userNames])
  _classroom = c_char_p(_sanitizeClassroomName(classroomName))
  _question = c_char_p(_sanitizeQuestionText(questionText))
  _choices, _ichoices = _makeCArray([_sanitizeChoice(choice) for choice in choices])
  _users, _iusers = _makeCArray(userNames)
  return _interruptableDllCall(dll.multiselect1, _classroom, c_int(len(classroomName) + 1), _question, c_int(len(questionText) + 1), _choices, _ichoices, c_int(len(choices)), _users, _iusers, c_int(len(userNames)), c_int(timeout), c_int(concurrentAnswers), c_bool(shortcut))


def yesno(classroomName, questionText, userNames = [], timeout = DEFAULT_TIMEOUT, shortcut = False):
  ''' Allows the user to choose between yes and no. There is no correction possible.
      Parameters are same as for multichoice, plus:
  '''
  assert 1 <= len(_sanitizeClassroomName(classroomName)) <= 8
  assert 2 <= len(_sanitizeQuestionText(questionText)) <= MAXTEXT
  assert all([1 <= len(user) <= 15 and _onlyDigits(user) for user in userNames])
  _classroom = c_char_p(_sanitizeClassroomName(classroomName))
  _question = c_char_p(_sanitizeQuestionText(questionText))
  _users, _iusers = _makeCArray(userNames)
  return _interruptableDllCall(dll.yesno1, _classroom, c_int(len(classroomName) + 1), _question, c_int(len(questionText) + 1), _users, _iusers, c_int(len(userNames)), c_int(timeout), c_bool(shortcut))


def decimal(classroomName, questionText, userNames = [], timeout = DEFAULT_TIMEOUT, shortcut = False):
  ''' Allow the user to enter decimal number including negative sign and decimal point.

      Parameters are same as for multichoice, plus:
  '''
  assert 1 <= len(_sanitizeClassroomName(classroomName)) <= 8
  assert 2 <= len(_sanitizeQuestionText(questionText)) <= MAXTEXT
  assert all([1 <= len(user) <= 15 and _onlyDigits(user) for user in userNames])
  _classroom = c_char_p(_sanitizeClassroomName(classroomName))
  _question = c_char_p(_sanitizeQuestionText(questionText))
  _users, _iusers = _makeCArray(userNames)
  return {k: float(v) for k, v in _interruptableDllCall(dll.decimal1, _classroom, c_int(len(classroomName) + 1), _question, c_int(len(questionText) + 1), _users, _iusers, c_int(len(userNames)), c_int(timeout), c_bool(shortcut))}


def getError():
  ''' Return a 3-tuple from smart response driver (statusCode, statusString, detailString). '''
  error = c_void_p()
  try:
    status = dll.getErrorCode(error)
    if status == 0: return (0, None, None)
    statusLen, detailLen = c_int(), c_int()
    dll.getErrorLengthes(error, byref(statusLen), byref(detailLen))
    statusText, detailText = c_char_p(statusLen), c_char_p(detailLen)
    dll.getErrorStatus(error, byref(statusText), byref(detailText))
    return (status, _decode_from_c(statusText.value), _decode_from_c(detailText.value))
  finally:
    dll.releaseError(error)


if __name__ == '__main__':
  import doctest
  doctest.testmod()
