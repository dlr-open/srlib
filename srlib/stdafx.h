// Copyright (c) 2018. Deutsches Zentrum fuer Luft- und Raumfahrt (DLR). All rights reserved.  http://www.dlr.de/ly
// This Source Code Form is subject to the terms of the Apache License, Version 2.0. If a copy of the license was not distributed with this file, You can obtain one at http://www.apache.org/licenses/LICENSE-2.0

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN  // don't include rarely used Windows stuff
#include <windows.h>
#define VC_EXTRALEAN		     // Exclude rarely-used stuff from Windows headers

#include <smartresponsesdk.h>
#include <smartresponseapiversioning.h>
