# Copyright (c) 2018. Deutsches Zentrum fuer Luft- und Raumfahrt (DLR). All rights reserved.  http://www.dlr.de/ly
# This Source Code Form is subject to the terms of the Apache License, Version 2.0. If a copy of the license was not distributed with this file, You can obtain one at http://www.apache.org/licenses/LICENSE-2.0

import srlib, subprocess

if __name__ == '__main__':
  ''' Simple voting example. '''
  print("Read out loud: Will we have snow during the winter holidays in Germany this year?  A = Yes, B = No")

  ans = srlib.yesno("Holidays", "Will we have snow over Christmas this year?", [], timeout = 300)

  with open(r"D:\votes.csv", "w") as fd:
    fd.write("Yes;No\n%d;%d\n" % (len([_ for _ in ans.values() if _ == 'A']), len([_ for _ in ans.values() if _ == 'B'])))
  subprocess.Popen(r'start excel "D:\votes.csv"', shell = True, bufsize = 1).wait()
