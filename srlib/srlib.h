// Copyright (c) 2018. Deutsches Zentrum fuer Luft- und Raumfahrt (DLR). All rights reserved.  http://www.dlr.de/ly
// This Source Code Form is subject to the terms of the Apache License, Version 2.0. If a copy of the license was not distributed with this file, You can obtain one at http://www.apache.org/licenses/LICENSE-2.0

#pragma once

#include <vector>

using namespace std; // for strings

#define INDEX(collection, iter) (iter - collection.begin()) // computes the (enumerate) index in a collection iteration

#ifdef __cplusplus
extern "C" {
#endif

#define DLL_EXPORT __declspec(dllexport)

// Utility functions
extern DLL_EXPORT const int getHardwareMode(void); // Detect and return connected hardware receiver code (allows to recognize PE/LE/XE systems).
extern DLL_EXPORT void printHardwareProperties(void); // Display device limitations TODO return them in an array for the wrapper to know the limitations

// Clicker functions (C-style)
extern DLL_EXPORT const int multichoice1(const char* markerFile, const char* classroomName, const int clen, const char* questionText, const int qlen, const char** choices, const int* clens, const int clennum, const char** studentIds, const int* slens, const int slennum, const int timeout, const bool allowCorrections);
extern DLL_EXPORT const int multiselect1(const char* markerFile, const char* classroomName, const int clen, const char* questionText, const int qlen, const char** choices, const int* clens, const int clennum, const char** studentIds, const int* slens, const int slennum, const int timeout, const unsigned int maxAnswersNum, const bool allowCorrections);
extern DLL_EXPORT const int yesno1(const char* markerFile, const char* classroom, const int clen, const char* questionText, const int qlen, const char** studentIds, const int* slens, const int slennum, const int timeout, const bool allowCorrections);
extern DLL_EXPORT const int decimal1(const char* markerFile, const char* classroomName, const int clen, const char* questionText, const int qlen, const char** studentIds, const int * slens, const int slennum, const int timeout, const bool allowCorrections);

// Clicker functions (C++-style)
extern DLL_EXPORT const int multichoice(const string markerFile, const string classroom, const string questionText, const vector<string> choices, const vector<string> studentIds, const int timeout, const bool allowCorrections);
extern DLL_EXPORT const int multiselect(const string markerFile, const string classroomName, const string questionText, const vector<string> choices, const vector<string> studentIds, const int timeout, const unsigned int maxAnswersNum, const bool allowCorrections);
extern DLL_EXPORT const int yesno(const string markerFile, const string classroom, const string questionText, const vector<string> studentIds, const int timeout, const bool allowCorrections);
extern DLL_EXPORT const int decimal(const string markerFile, const string classroom, const string questionText, const vector<string> studentIds, const int timeout, const bool allowCorrections);

// Teacher-side callbacks
static void __stdcall onConnect(void *arg);
static void __stdcall onDisconnect(void *arg);
static void __stdcall onConnectionFail(void *arg);
static void __stdcall onReceiverPluggedIn(void *arg);
static void __stdcall onReceiverPluggedOut(void *arg);
static void __stdcall onReceiverReady(void *arg);
static void __stdcall onClassStart(void *arg);
static void __stdcall onClassStop(void *arg);
static void __stdcall onClassStartFail(void *arg);
static void __stdcall onClassStopFail(void *arg);
static void __stdcall onQuestionStart(void *arg);
static void __stdcall onQuestionStop(void *arg);
static void __stdcall onQuestionStartFail(void *arg);
static void __stdcall onQuestionStopFail(void *arg);
static void __stdcall onQuestionsetStart(void *arg);
static void __stdcall onQuestionsetStop(void *arg);
static void __stdcall onQuestionsetStartFail(void *arg);
static void __stdcall onQuestionsetStopFail(void *arg);
static void __stdcall onModeSwitch(void *arg);
static void __stdcall onModeSwitchFail(void *arg);

// Remote state change callbacks
static void __stdcall onClickerConnect(char* studentId, void *arg); // anonymous: MAC, otherwise sign-in ID
static void __stdcall onClickerDisconnect(char *studentId, void *arg); // anonymous: MAC, otherwise sign-in ID
static void __stdcall onQuestion(char *studentId,void *arg);
static void __stdcall onQuestionCancel(char *studentId,void *arg);
static void __stdcall onSubmit(char *studentId,void *arg);

// Remote response callback
static void __stdcall onClickerResponse(char* studentId, char* questionId, char* response, void* aContext);

// Error functions
extern DLL_EXPORT const smartresponse_errorinfoV1_t* getError(void); // Only get error object.

extern DLL_EXPORT void releaseError(const smartresponse_errorinfoV1_t* error); // Release error object.

extern DLL_EXPORT const int getErrorCode(const smartresponse_errorinfoV1_t *error); // Returns 0 if OK, -1 on hardware failure, 3xx/4xx/5xx on connection errors.

extern DLL_EXPORT void getErrorLengthes(const smartresponse_errorinfoV1_t *error, unsigned int *statusSize, unsigned int *messageSize); // Sets buffer sizes for errors status and message strings.

extern DLL_EXPORT void getErrorStatus(const smartresponse_errorinfoV1_t *error, char *statusText, char *detailText); // Return status and message strings as UTF-8 bytes.

extern DLL_EXPORT void printError(void); // Print last error message on console, if any.

#ifdef __cplusplus
}
#endif
