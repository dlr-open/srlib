// Copyright (c) 2018. Deutsches Zentrum fuer Luft- und Raumfahrt (DLR). All rights reserved.  http://www.dlr.de/ly
// This Source Code Form is subject to the terms of the Apache License, Version 2.0. If a copy of the license was not distributed with this file, You can obtain one at http://www.apache.org/licenses/LICENSE-2.0

// stdafx.cpp : Quelldatei, die nur die Standard-Includes einbindet
// *.pch      : sind vorkompilierte Header
// stdafx.obj : enth�lt die vorkompilierten Typinformationen

#include "stdafx.h"

// TODO: Auf zus�tzliche Header verweisen, die in STDAFX.H und nicht in dieser Datei erforderlich sind.
